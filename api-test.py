import gitlab

gl = gitlab.Gitlab("http://gitlab.com")
project = gl.projects.get("28262778")

pipelines = project.pipelines.list()
pipeline = pipelines[0]
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/pipelines_and_jobs.html#id10
test_report = pipeline.test_report.get()


# but this works:
# https://gitlab.com/api/v4/projects/28262778/pipelines/339877782/test_report
